# Bowling Game Kata (di [UncleBob](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata))

Il gioco del *bowling* divide la gara di ciascun giocatore in 10 **frame**: in
ogni *frame* il giocatore ha 2 possibilità di abbattere i 10 birilli (*pin*). Il
punteggio ottenuto nel *frame* è il numero di birilli abbattuti, maggiorato di
un premio per gli **spare** e gli **strike**.

Uno *spare* si verifica quando vengono abbattuti 10 birilli usando i 2
tentativi. In questo caso il premio è il numero di birilli abbattuto con il tiro
(*roll*) seguente (effettuato nel prossimo *frame*).

Uno *strike* si verifica quando vengono abbattuti 10 birilli al primo tentativo:
in questo caso il secondo tiro del *frame* non viene effettuato. Il premio per
lo *strike* è il numero di birilli abbattuto con i 2 tiri seguenti (effettuati
nel prossimo *frame*).

Se uno *strike* o uno *spare* si verificano nel decimo *frame*, il giocatore ha
diritto ai tiri necessari ad acquisire il premio relativo. Il decimo *frame* può
quindi dare luogo a un massimo di 3 tiri. Il punteggio massimo ottenibile è 300
punti.

```java
class Game {
 public void roll(int pins){}
 public int score(){}
}
```

# Esercizio 1 

(tempo stimato 15')

Il progetto viene già fornito con i test di unità di Uncle Bob
(`BowlingGameTest.java`).

## *Setup* di Gradle

Allestire correttamente l'ambiente di progetto con Gradle, in modo che sia
possibile compilare con Java ed effettuare i test con la libreria `junit`. Alla
fine dovrete ottenere:

- le *directory* previste dalle convenzioni Gradle per i progetti Java, che
  userete per i *file* `.java` che compongono il progetto
- un opportuno *file* `build.gradle` che utilizza i *plugin* adatti al progetto
  e permette di ottenere il fallimento dei test forniti con il comando `gradle
  test`

Suggerimento:
[*Quickstart* Gradle per Java](https://docs.gradle.org/3.2.1/userguide/tutorial_java_projects.html)

## *Setup* del *repository*

Aggiungere il *wrapper* di Gradle al *repository* git.

Vedi [Documentazione *wrapper*](https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:wrapper_generation)

## Setup del *repository* GitLab

Configurare la *continuous integration* (CI) su GitLab.

## Svolgimento
Si vedano
[build.gradle](https://gitlab.com/ItachiSan/lab03/blob/master/build.gradle)
e 
[.gitlab-ci.yml](https://gitlab.com/ItachiSan/lab03/blob/master/.gitlab-ci.yml)


# Esercizio 2

(tempo stimato 1h)

## Superare i test

Implementare la classe `Game` in modo da superare i test.

## Un programma *client* per `Game`

Scrivere un programma (con un `main`) che utilizzi la classe `Game` per simulare
una partita. Il programma deve stampare l'evoluzione del punteggio con la
libreria [`SLF4J`](http://www.slf4j.org/).

- utilizzare il
  *plugin*
  [`application`](https://docs.gradle.org/current/userguide/application_plugin.html).
- creare un rapporto di copertura con il
  *plugin*
  [`jacoco`](https://docs.gradle.org/3.2.1/userguide/jacoco_plugin.html).
- aggiungere la possibilità di scaricare i rapporti generati da `jacoco` (Vedi:
  https://docs.gitlab.com/ce/ci/yaml/README.html#artifacts).
- Il *coverage* può essere visualizzato impostando l'espressione regolare
  `Total.+>(\d+)\%` nel `Test coverage parsing` dei `Pipelines settings`
  e aggiungendo un comando che stampi su *standard output* l'`index.html` creato
  da `jacoco`.

## Svolgimento
Si vedano i files in
[src/main/java](https://gitlab.com/ItachiSan/lab03/tree/master/src/main/java).

Build status: [![build status](https://gitlab.com/ItachiSan/lab03/badges/master/build.svg)](https://gitlab.com/ItachiSan/lab03/commits/master)

Coverage: [![coverage report](https://gitlab.com/ItachiSan/lab03/badges/master/coverage.svg)](https://gitlab.com/ItachiSan/lab03/commits/master)

# Esercizio 3

(tempo stimato 1h)

Scrivere un nuovo *plugin* che permetta di compilare (*melt*) e finalizzare
(*finish_freezing*) un programma Eiffel (sottoprogetto `eiffel`, per includerlo
aggiungere `include 'eiffel'` in `settings.gradle`). Si noti che:

- Tutti gli strumenti Eiffel Studio necessitano della corretta impostazione delle variabili d'ambiente:
  `ISE_EIFFEL` (p.es. `/opt/Eiffel_16.05`) e `ISE_PLATFORM` (p.es. `linux-x86-64`).
- Il compilatore `ec` e il finalizzatore `finish_freezing` si trovano nella
  *directory* `$ISE_EIFFEL/studio/spec/$ISE_PLATFORM/bin`
- Le opzioni dei *tool* possono essere elencate con l'opzione `-help`
- La finalizzazione dipende dalla corretta compilazione
- Può essere opportuno fare creare tutti i *file* di appoggio in una opportuna
  *directory* di *build*, in modo che sia facile avere anche un *task* di
  cancellazione (`clean`).
- La documentazione sulla scrittura di *plugin*
  è [qui](https://docs.gradle.org/current/userguide/custom_plugins.html) e un
  esempio è contenuto nella *directory* `buildSrc/src/main/groovy`. Il *plugin*
  d'esempio può essere attivato aggiungendo `apply plugin: GreetingPlugin` a
  `build.gradle`, eventualmente configurando `greeting.message`.
  
