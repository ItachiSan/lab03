import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Runner {
    public static void main(String[] args) {
        // Generate the pins down
        Random r = new Random();
        // Use SLF4J for logging
        final Logger logger = LoggerFactory.getLogger(Runner.class);
        // Variables needed for keeping track of the game
        int roll = 0, previous_roll = 0;
        Game g = new Game();

        // Play the game!
        for (int played_turn = 0; played_turn < Game.TURNS_NUM; played_turn++) {
            // Throw the first ball
            logger.info("Turn " + (played_turn+1) + " (score: " + g.score() + ")");
            roll = r.nextInt(Game.ALL_PINS_DOWN + 1);
            g.roll(roll);

            // If we don't get a strike...
            if (roll != Game.ALL_PINS_DOWN) {
                logger.info("First roll: " + roll);
                // trow another time
                previous_roll = roll;
                roll = r.nextInt(Game.ALL_PINS_DOWN + 1 - previous_roll);
                g.roll(roll);
                logger.info("Second roll: " + roll);
                // If we have a spare, say it!
                if ((roll + previous_roll) == Game.ALL_PINS_DOWN)
                    logger.info("Spare!");
            } else {
                logger.info("First roll: " + Game.ALL_PINS_DOWN);
                logger.info("Strike!");
            }

            // Handle the last frame properly!
            if (played_turn == Game.TURNS_NUM - 1) {
                if (roll == Game.ALL_PINS_DOWN) {
                    // Trow other 2 times!
                    roll = r.nextInt(Game.ALL_PINS_DOWN + 1);
                    g.roll(roll);
                    logger.info("Second roll: " + roll);
                    roll = r.nextInt(
                        roll == Game.ALL_PINS_DOWN ?
                        (Game.ALL_PINS_DOWN + 1) :
                        (Game.ALL_PINS_DOWN + 1 - roll)
                    );
                    g.roll(roll);
                    logger.info("Third roll: " + roll);

                } else if ((roll + previous_roll) == Game.ALL_PINS_DOWN) {
                    roll = r.nextInt(Game.ALL_PINS_DOWN + 1);
                    g.roll(roll);
                    logger.info("Third roll: " + roll);
                }
            }
        }

        // Finished!
        logger.info("Final score: " + g.score());
    }
}