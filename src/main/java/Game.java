class Game {
    final static int TURNS_NUM = 10;
    final static int ALL_PINS_DOWN = 10;
    final static int ROLLS_MAX_NUM = 21;
    int[] rolls;
    int roll_number;

    Game() {
        rolls = new int[ROLLS_MAX_NUM];
        roll_number = 0;
    }

    void roll(int pins) {
        rolls[roll_number++] += pins;
    }

    int score() {
        int final_score = 0;
        for(int i = 0, j = 0; i < TURNS_NUM; i++) {
            if (isStrike(j)) {
                final_score += ALL_PINS_DOWN + bonusStrike(j);
                j += 1;
            }
            else if (isSpare(j)) {
                final_score += ALL_PINS_DOWN + bonusSpare(j);
                j += 2;
            }
            else {
                final_score += rolls[j] + rolls[j+1];
                j += 2;
            }
        }
        return final_score;
    }

    boolean isStrike(int i) {
        return rolls[i] == ALL_PINS_DOWN;
    }
    boolean isSpare(int i) {
        return (rolls[i] + rolls[i+1]) == ALL_PINS_DOWN;
    }

    int bonusStrike(int i) {
        // The bouns for the strike are the two next rolls.
        return rolls[i+1] + rolls[i+2];
    }

    int bonusSpare(int i) {
        // The bouns for the strike is the next roll, in the next turn.
        return rolls[i+2];
    }
}